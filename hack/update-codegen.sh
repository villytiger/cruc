#!/usr/bin/env bash

# Copyright 2019 Ilya Lyubimov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -o errexit
set -o nounset
set -o pipefail

TOPDIR=$(dirname "${BASH_SOURCE[0]}")/..

bash "$TOPDIR/vendor/k8s.io/code-generator/generate-groups.sh" "deepcopy,client,informer,lister" \
     github.com/villytiger/cruc/pkg/api github.com/villytiger/cruc/pkg/api cruc:v1alpha1 \
     --go-header-file "$TOPDIR/hack/boilerplate.go.txt" --output-base "$TOPDIR/hack/tmp" \
    || exit $?

cp -r "$TOPDIR/hack/tmp/github.com/villytiger/cruc/pkg" "$TOPDIR" || exit $?
rm -r "$TOPDIR/hack/tmp" || exit $?
