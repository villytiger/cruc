# Customizable webhook for Kubernetes

## Installation

```shell
helm repo add villytiger https://villytiger.gitlab.io/helm-charts
helm install -n cruc cruc villytiger/cruc
```

## Example

Set runAsUser property for every pod created with cruc=test annotation:
```shell
kubectl apply -f - <<EOF
apiVersion: "cruc.vtn.su/v1alpha1"
kind: PodPatch
metadata:
  name: test
spec:
  annotations:
    cruc: test
  patch:
  - op: replace
    path: /spec/securityContext/runAsUser
    value: 1000
EOF
```

Create test pod:
```shell
kubectl apply -f - <<EOF
apiVersion: v1
kind: Pod
metadata:
  name: cruc-test
  annotations:
    cruc: test
spec:
  containers:
  - name: test
    image: alpine
    command: ["sleep", "24h"]
EOF
```

Check that patch was applied (must print 1000):
```shell
kubectl get pod cruc-test -o jsonpath="{.spec.securityContext.runAsUser}{'\n'}"
```
