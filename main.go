// Copyright 2019 Ilya Lyubimov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"context"
	"flag"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	cache "k8s.io/client-go/tools/cache"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/klog"
	"k8s.io/client-go/tools/clientcmd"

	cruc "github.com/villytiger/cruc/pkg/api/cruc/v1alpha1"
	clientset "github.com/villytiger/cruc/pkg/api/clientset/versioned"
)

func watchConfig(c cache.Getter, store cache.Store) chan struct{} {
	lw := cache.NewListWatchFromClient(c, "podpatches", metav1.NamespaceAll, fields.Everything())
	r := cache.NewReflector(lw, &cruc.PodPatch{}, store, 0)
	stopCh := make(chan struct{})
	klog.Info("Start watching PodPatch resources")
	go r.Run(stopCh)
	return stopCh
}

func main() {
	klog.InitFlags(nil)
	masterURL := flag.String("master", "", "The address of the Kubernetes API server. Overrides any value in kubeconfig. Only required if out-of-cluster.")
	configPath := flag.String(clientcmd.RecommendedConfigPathFlag, "", "Path to a kubeconfig. Only required if out-of-cluster.")
	tlsPrivateKeyFile := flag.String("tls-private-key-file", "/app/tls/tls.key", "Path to a file with the tls private key.")
	tlsCertFile := flag.String("tls-cert-file", "/app/tls/tls.crt", "Path to a file with the tls certificate and intermediate certificates.")
	flag.Parse()

	config, err := clientcmd.BuildConfigFromFlags(*masterURL, *configPath)
	if err != nil {
		klog.Fatalf("Error building kubeconfig: %s", err.Error())
	}

	client, err := clientset.NewForConfig(config)
	if err != nil {
		klog.Fatalf("Error building clientset: %s", err.Error())
	}

	store := cache.NewStore(cache.MetaNamespaceKeyFunc)
	stopCh := watchConfig(client.CrucV1alpha1().RESTClient(), store)
	defer close(stopCh)

	signalCh := make(chan os.Signal)
	signal.Notify(signalCh, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)

	servemux := http.NewServeMux()
	servemux.Handle("/mutate", &Handler{Store: store})
	servemux.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) { w.WriteHeader(http.StatusOK) })
	server := &http.Server{Addr: ":8443", Handler: servemux }
	go func() {
		klog.Info("Start listening webhook socket")
		err := server.ListenAndServeTLS(*tlsCertFile, *tlsPrivateKeyFile)
		if err != http.ErrServerClosed {
			klog.Errorf("Error listening HTTPS requests: %s", err.Error())
			signalCh <- os.Interrupt
		}
	}()
	defer server.Shutdown(context.Background())

	<-signalCh
}
