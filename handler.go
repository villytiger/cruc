// Copyright 2019 Ilya Lyubimov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	admissionv1 "k8s.io/api/admission/v1"
	corev1 "k8s.io/api/core/v1"
	apiextensionsv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	"k8s.io/apimachinery/pkg/runtime"
	k8sjson "k8s.io/apimachinery/pkg/runtime/serializer/json"
	"k8s.io/client-go/tools/cache"
	"k8s.io/klog"

	cruc "github.com/villytiger/cruc/pkg/api/cruc/v1alpha1"
)

var scheme *runtime.Scheme
var serializer *k8sjson.Serializer

func init() {
	scheme = runtime.NewScheme()
	corev1.AddToScheme(scheme)
	admissionv1.AddToScheme(scheme)
	serializer = k8sjson.NewSerializer(k8sjson.DefaultMetaFactory, scheme, scheme, false)
}

type Handler struct {
	Store cache.Store
}

func (h *Handler) mutatePod(pod *corev1.Pod) []apiextensionsv1.JSON {
	klog.V(1).Infof("Mutating %s/%s pod", pod.Namespace, pod.Name)

	var patch []apiextensionsv1.JSON
	for _, obj := range h.Store.List() {
		podPatch := obj.(*cruc.PodPatch)

		match := true
		for name, value := range podPatch.Spec.Annotations {
			if v, ok := pod.Annotations[name]; !ok || value != v {
				match = false
				break
			}
		}

		if match {
			klog.V(1).Infof("Applying \"%s\" patch for \"%s/%s\" pod",
				podPatch.Name, pod.Namespace, pod.Name)
			patch = append(patch, podPatch.Spec.Patch...)
		}
	}

	return patch
}

func (h *Handler) servePodRequest(request *admissionv1.AdmissionRequest) admissionv1.AdmissionResponse {
	response := admissionv1.AdmissionResponse{UID: request.UID, Allowed: true}

	pod := corev1.Pod{}
	if _, _, err := serializer.Decode(request.Object.Raw, nil, &pod); err != nil {
		msg := fmt.Sprintf("Error parsing Pod in a request: %s", err.Error())
		klog.Errorf(msg)
		response.Allowed = false
		response.Result.Code = 500
		response.Result.Message = msg
		return response
	}
	pod.Namespace = request.Namespace

	patch := h.mutatePod(&pod)
	if len(patch) == 0 {
		return response
	}

	patchValue, err := json.Marshal(patch)
	if err != nil {
		msg := fmt.Sprintf("Error serializing patch: %s", err.Error())
		klog.Error(msg)
		response.Allowed = false
		response.Result.Code = 500
		response.Result.Message = msg
		return response
	}

	patchType := admissionv1.PatchTypeJSONPatch
	response.PatchType = &patchType
	response.Patch = patchValue

	return response
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "Invalid method, expect POST", http.StatusMethodNotAllowed)
		return
	}

	if contentType := r.Header.Get("Content-Type"); contentType != "application/json" {
		msg := fmt.Sprintf("Invalid Content-Type=%s, expect application/json", contentType)
		klog.Error(msg)
		http.Error(w, msg, http.StatusUnsupportedMediaType)
		return
	}

	if r.Body == nil {
		msg := "Error parsing request: empty body"
		klog.Error(msg)
		http.Error(w, msg, http.StatusBadRequest)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		msg := fmt.Sprintf("Error reading body: %s", err.Error())
		klog.Error(msg)
		http.Error(w, msg, http.StatusBadRequest)
	}

	klog.V(2).Infof("Handling request: %s", body)

	request := admissionv1.AdmissionReview{}
	if _, _, err := serializer.Decode(body, nil, &request); err != nil {
		klog.Error(err.Error())
		http.Error(w, "cannot parse request", http.StatusBadRequest)
		return
	} else if request.Request == nil {
		klog.Error("empty request")
		http.Error(w, "empty request", http.StatusBadRequest)
		return
	}

	admissionResponse := h.servePodRequest(request.Request)
	response := admissionv1.AdmissionReview{Response: &admissionResponse}
	response.SetGroupVersionKind(admissionv1.SchemeGroupVersion.WithKind("AdmissionReview"))

	var responseBody bytes.Buffer
	if err := serializer.Encode(&response, &responseBody); err != nil {
		klog.Error(err.Error())
		return
	}

	klog.V(2).Infof("Sending response: %s", responseBody.String())

	if _, err := responseBody.WriteTo(w); err != nil {
		klog.Errorf("Error writing response body: %s", err.Error())
	}
}
